#!/bin/bash

################################################################################
# Filename: DB_apply_patch.sh
# ------------------------------------------------------------------------------
# Contact: Tyler Crofoot
# ------------------------------------------------------------------------------
# Description:
# ------------------------------------------------------------------------------
# Applies Patches to databases and verifies them
################################################################################

###############
# Variables
###############
DATE=$(date +"%d-%b-%Y-%I-%M-%p")
HOST=$(hostname)
SRCDIR=$ORACLE_HOME
FILENAME=$HOST-OraHome-bkp-$DATE.tar.gz
DATABASENUM=$(ps -ef | grep pmon | wc -l)
PSU_PATCH_PATH=$1
OJVM_PATCH_PATH=$2
JDK_PATCH_PATH=$3
DB_VERSION=$4
DESTINATION_DIRECTORY=$5
PATHS=("$PSU_PATCH_PATH" "$OJVM_PATCH_PATH" "$JDK_PATCH_PATH")
DB_PACKAGE=("PSU" "OJVM" "JDK")
LEN=${#PATHS[@]}
ERROR_FILE="/home/oracle/invalid_objects.txt"
SELECT_STATEMENT="select comp_id, comp_name, version, status from dba_registry where status="
DB_LISTENER=$(ps -ef | grep tns| grep LISTENER* | awk '{print $9}')

border()
{
    title="| $1 |"
    edge=$(echo "$title" | sed 's/./-/g')
    echo "$edge"
    echo "$title"
    echo "$edge"
}


################################################################################
# Taking the backup with compression
################################################################################
echo -en '\n'
border "Taking Backup Of Database"
echo -en '\n'

tar -cpzf $DESTINATION_DIRECTORY/$FILENAME $SRCDIR

echo $FILENAME
sleep 1m


###########################################
# Create error file
###########################################
/usr/bin/touch $ERROR_FILE
/usr/bin/chmod 644 $ERROR_FILE



################################################################################
# Shutdown the database LISTENER on server
################################################################################
echo -en '\n'
border "Shutdown Database LISTENER"
echo -en '\n'

/u01/app/oracle/product/"$DB_VERSION"/dbhome_1/bin/lsnrctl stop $DB_LISTENER


################################################################################
# Show status of database LISTENER on server
################################################################################
echo -en '\n'
border "Show Status of Database LISTENER"
echo -en '\n'

/u01/app/oracle/product/"$DB_VERSION"/dbhome_1/bin/lsnrctl status $DB_LISTENER


################################################################################
# Shut down the database's on server
################################################################################
echo -en '\n'
border "Shutdown Database"
echo -en '\n'

for SID in `cat /etc/oratab |grep ":"|awk -F: '{print $1}'|grep -v "#"`
do

	border $SID
        export ORACLE_SID=$SID
        /u01/app/oracle/product/"$DB_VERSION"/dbhome_1/bin/sqlplus -s / as sysdba << EOF

	shutdown immediate;
	exit
EOF
done
################################################################################
# Check that the database's shutdown before moving to apply
################################################################################
echo -en '\n'
border "Checking What Database Services Are Running"
echo -en '\n'

if [[ $DATABASENUM -ge 1 ]] ; then
    echo "All Databases appear to be shutdown. Moving on to apply patches"
else
    echo "Cannot apply patches Databases are still running"
exit 1
fi

################################################################################
# Show the number of pmon services still running.
################################################################################
echo -en '\n'
border "Database Services Still Running"
echo -en '\n'

echo "Number of DB Services runninig: $DATABASENUM"

ps -ef | grep pmon


################################################################################
# Apply new PSU | OJVM | & JDK patches
################################################################################
for (( i=0; i<$LEN; i++ ));
do
    echo -en '\n'
    border "Apply ${DB_PACKAGE[$i]} Patches"
    echo -en '\n'
    cd ${PATHS[$i]} && yes | /u01/app/oracle/product/"$DB_VERSION"/dbhome_1/OPatch/opatch apply
done
################################################################################
# Verify new PSU | OJVM | & JDK patches applied
################################################################################
echo -en '\n'
border "List ${DB_PACKAGE[$i]} Inventory"
echo -en '\n'

cd ${PATHS[0]} && yes | /u01/app/oracle/product/"$DB_VERSION"/dbhome_1/OPatch/opatch lsinventory


################################################################################
# Start the database LISTENER on server
################################################################################
echo -en '\n'
border "Start Database LISTENER"
echo -en '\n'

/u01/app/oracle/product/19.0.0/dbhome_1/bin/lsnrctl start $DB_LISTENER


################################################################################
# Verify Databases are healthy before post patch
################################################################################
echo -en '\n'
border "Check Database health before post patch"
echo -en '\n'

for SID in `cat /etc/oratab |grep ":"|awk -F: '{print $1}'|grep -v "#"`
do

        border $SID
        export ORACLE_SID=$SID
        /u01/app/oracle/product/"$DB_VERSION"/dbhome_1/bin/sqlplus -s / as sysdba << EOF


	startup upgrade;
	@?/rdbms/admin/utlrp.sql;
	col COMP_NAME for a35;
	col COMP_ID for a10;
	set linesize 300;
	spool /home/oracle/invalid_objects.txt;
	select comp_id, comp_name, version, status from dba_registry where status='INVALID';
	spool off;
	exit
EOF

###############
# Variables
###############
ERROR_HANDLE=$(cat $ERROR_FILE | grep 'INVALID' | wc -l)
ERROR_OUTPUT=$(cat $ERROR_FILE | grep 'INVALID')



if [[ $ERROR_HANDLE == 1 &&  $ERROR_OUTPUT == *$SELECT_STATEMENT* ]] ; then
   border "NO INVALID OBJECTS FOUND"
elif [[ $ERROR_HANDLE == 0 ]] ; then
   border "NO INVALID OBJECTS FOUND"
else
   border "INVALID OBJECTS FOUND! FIX OBJECTS BEFORE RUNNING POST PATCH SCRIPT"
   exit 1
fi
done
