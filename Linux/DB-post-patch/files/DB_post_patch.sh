#!/bin/bash

################################################################################
# Filename: DB_post_patch.sh
# ------------------------------------------------------------------------------
# Contact: Tyler Crofoot
# ------------------------------------------------------------------------------
# Description:
# ------------------------------------------------------------------------------
# Post-Patching checks on databases
################################################################################


###############
# Variables
###############
DB_VERSION=$1
DB_LISTENER=$(ps -ef | grep tns| grep LISTENER* | awk '{print $9}')
TNS_STATUS=$(ps -ef | grep tns | awk '{print $9}')
DB_STATUS=$(ps -ef | grep pmon | awk '{print $8}')

border()
{
    title="| $1 |"
    edge=$(echo "$title" | sed 's/./-/g')
    echo "$edge"
    echo "$title"
    echo "$edge"
}


################################################################################
# Start the database LISTENER on server
################################################################################
echo -en '\n'
border "Start Database LISTENER"
echo -en '\n'

/u01/app/oracle/product/19.0.0/dbhome_1/bin/lsnrctl start $DB_LISTENER


################################################################################
# Sqlplus post-patching checks & bringing Databases up
################################################################################
echo -en '\n'
border "Running Post-Patch SQL Commands & Starting Database"
echo -en '\n'

for SID in `cat /etc/oratab |grep ":"|awk -F: '{print $1}'|grep -v "#"`
do
        border $SID
        export ORACLE_SID=$SID
        /u01/app/oracle/product/"$DB_VERSION"/dbhome_1/OPatch/datapatch -verbose
        /u01/app/oracle/product/"$DB_VERSION"/dbhome_1/bin/sqlplus -s "/ as sysdba" << EOF

	select name from v\$database;
	shut immediate;
	sleep 45
	startup;
	select count(*) from dba_objects where status='INVALID';
	@?/rdbms/admin/utlrp.sql;
	select count(*) from dba_objects where status='INVALID';
	col ACTION_TIME for a33;
	col DESCRIPTION for a35;
	col action for a12;
	col version for a11;
	set lines 300;
	select PATCH_ID,PATCH_UID,ACTION,STATUS,INSTALL_ID,ACTION_TIME,DESCRIPTION from dba_registry_sqlpatch;
	col comp_name for a55;
	set lines 300;
	select comp_name, version, status from dba_server_registry;
	exit
EOF

echo -en '\n'
border "Status of Running TNS Services"
echo -en '\n'

echo $TNS_STATUS

echo -en '\n'
border "Status of Running Databases"
echo -en '\n'

echo $DB_STATUS

done
