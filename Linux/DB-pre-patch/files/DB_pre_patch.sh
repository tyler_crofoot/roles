#!/bin/bash

################################################################################
# Filename: DB_pre_patch.sh
# ------------------------------------------------------------------------------
# Contact: Tyler Crofoot
# ------------------------------------------------------------------------------
# Description:
# ------------------------------------------------------------------------------
# Pre-Patching checks on databases
################################################################################


###############
# Variables
###############
PSU_PATCH_PATH=$1
OJVM_PATCH_PATH=$2
JDK_PATCH_PATH=$3
DB_VERSION=$4
DB_PACKAGE=("PSU" "OJVM" "JDK")
PATHS=("$PSU_PATCH_PATH" "$OJVM_PATCH_PATH" "$JDK_PATCH_PATH")
LEN=${#PATHS[@]}

border()
{
    title="| $1 |"
    edge=$(echo "$title" | sed 's/./-/g')
    echo "$edge"
    echo "$title"
    echo "$edge"
}


sleep 1m
################################################################################
# Sqlplus pre-patching checks
################################################################################
for SID in `cat /etc/oratab |grep ":"|awk -F: '{print $1}'|grep -v "#"`
do
        border $SID
        export ORACLE_SID=$SID
        /u01/app/oracle/product/"$DB_VERSION"/dbhome_1/bin/sqlplus -s "/ as sysdba" << EOF

	select count(*) from dba_objects where status='INVALID';
	@?/rdbms/admin/utlrp.sql;
	select count(*) from dba_objects where status='INVALID';
	exit
EOF
done
################################################################################
# Pre-check PSU | OJVM | & JDK packages
################################################################################
for (( i=0; i<$LEN; i++ ));
do
     echo -en '\n'
     border "Pre Patch Check of ${DB_PACKAGE[$i]} Packages"
     echo -en '\n'
     cd ${PATHS[$i]} && yes | /u01/app/oracle/product/"$DB_VERSION"/dbhome_1/OPatch/opatch prereq CheckConflictAgainstOHWithDetail -ph ./
done
